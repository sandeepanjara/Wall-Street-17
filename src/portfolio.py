from tornado.gen import coroutine

from src.authenticate import authenticate
from src.base import BaseHandler


class PortfolioHandler(BaseHandler):
    @authenticate
    @coroutine
    def get(self, *args, **kwargs):
        user_id = int(self.get_argument('user_id'))
        document = yield self.db.investor.find_one({"_id": user_id}, {"transaction": {"$slice": -12}, "portfolio": 1})
        if document:
            portfolio = document["portfolio"]
            # transactions = sorted(document["transaction"], key=lambda x: x["timestamp"])
            for transaction in document["transaction"]:
                transaction["timestamp"] = transaction["timestamp"].__str__()[:19]
            self.respond({"portfolio": portfolio, "transaction": document["transaction"]}, 200)
        else:
            self.respond("user not found", 200)
