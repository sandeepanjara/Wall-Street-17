from bson import timestamp
from tornado.gen import coroutine

from src.authenticate import authenticate
from src.base import BaseHandler


class DashboardHandler(BaseHandler):
    @authenticate
    @coroutine
    def get(self, *args, **kwargs):
        # TODO
        # make graph separate collection
        db = self.settings['client'].wallstreet
        user_id = int(self.get_argument('user_id'))
        company_cursor = db.company.find()
        companies = list()
        while (yield company_cursor.fetch_next):
            company = company_cursor.next_object()
            companies.append(company)
        result = yield db.investor.find_one({"_id": user_id}, {"balance": 1, "name": 1})
        result["balance"] = round(result["balance"], 2)
        response = dict(companies=companies, investor=result)
        self.respond(response, 200)
