from datetime import datetime

from tornado.gen import coroutine

from src.base import BaseHandler


class LeaderBoard(BaseHandler):
    @coroutine
    def get(self, *args, **kwargs):
        end_time = datetime(2017, 3, 20, 17, 33)
        time = datetime.now()
        if time < end_time:
            self.respond("Before time", 555)
        else:
            rank = 0
            investors = list()
            previous = 0
            investors_cursor = self.db.investor.find({}, {"_id": 1, "name": 1, "balance": 1}).sort("balance", -
            1)
            while (yield investors_cursor.fetch_next):
                investor = investors_cursor.next_object()
                if investor["balance"] == previous:
                    investor["rank"] = rank
                    investors.append(investor)
                else:
                    rank += 1
                    investor["rank"] = rank
                    previous = investor["balance"]
                    investors.append(investor)
                investor["balance"] = round(investor["balance"], 2)
                del investor["_id"]
            self.respond(investors, 200)
