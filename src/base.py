import json
from datetime import datetime

from tornado.web import RequestHandler


class BaseHandler(RequestHandler):
    def initialize(self):
        self.set_header('Content-Type', 'application/json')
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Credentials", "false")
        self.set_header("Access-Control-Expose-Headers", "*")
        self.set_header("Access-Control-Allow-Methods", "*")
        self.set_header("Access-Control-Allow-Headers", "*")
        self.set_header("Access-Control-Allow-Headers", "accept, authorization")
        self._parsed = False
        self._response = None
        self.body = dict()
        self.db = self.settings["client"].wallstreet
        self.token = None

    def pars_body(self):
        if self._parsed is False:
            self.body = json.loads(self.request.body.decode())
            self._parsed = True
            return self.body
        else:
            return self.body

    def get_request_body_argument(self, key):
        try:
            if self._parsed is False:
                self.body = self.pars_body()
            return self.body[key]
        except KeyError:
            raise Exception(key + "is missing in request body")

    def respond(self, response, status_code):
        self._response = {"message": response, "status": status_code}
        data = json.dumps(self._response)
        self.write(data)

    def write_error(self, status_code, **kwargs):
        self.set_header('Content-Type', 'application/json')
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Credentials", "false")
        self.set_header("Access-Control-Expose-Headers", "*")
        self.set_header("Access-Control-Allow-Methods", "*")
        self.set_header("Access-Control-Allow-Headers", "*")
        self.set_header("Access-Control-Allow-Headers", "accept, authorization")

    def options(self, *args, **kwargs):
        self.write_error(200)
