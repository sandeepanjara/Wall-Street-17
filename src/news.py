from tornado.gen import coroutine

from src.authenticate import authenticate
from src.base import BaseHandler


class NewsHandler(BaseHandler):
    @authenticate
    @coroutine
    def get(self, *args, **kwargs):
        db = self.settings['client'].wallstreet
        user_id = int(self.get_argument('user_id'))
        news_cursor = db.news.find().limit(5).sort("timestamp", -1)
        news_list = list()
        while (yield news_cursor.fetch_next):
            news = news_cursor.next_object()
            news["timestamp"] = news["timestamp"].__str__()
            del news["_id"]
            news_list.append(news)
        investor = yield db.investor.find_one({"_id": user_id})
        if investor["broker"] is None:
            broker_news = ["Broker Not Assigned"]
        else:
            broker = investor["broker"]
            broker_news = yield db.broker.find_one({"_id": broker}, {"message": 1})
            broker_news = broker_news["message"]
            broker_news = sorted(broker_news, key=lambda x: x["timestamp"], reverse=True)
            for news in broker_news:
                news['timestamp'] = news["timestamp"].__str__()
        response = dict(broker_news=broker_news, news=news_list)
        self.respond(response=response, status_code=200)
