from datetime import datetime
from functools import wraps

from bson import ObjectId
from tornado.gen import coroutine


def authenticate(function):
    @coroutine
    @wraps(function)
    def wrapper(self, *args, **kwargs):
        token = self.request.headers.get("Authorization")
        self.token = token
        start_time = datetime(2017, 3, 21, 10, 31)
        end_time = datetime(2017, 3, 21, 15, 59)
        time = datetime.now()
        if token == "null":
            self.respond("Forbidden", 403)
        if token is None:
            self.respond("Forbidden", 403)
        else:
            try:
                token = ObjectId(token)
                result = yield self.db.user.find_one({"token": token}, {"token": 1})
                if result is not None:
                    if time < start_time:
                        self.respond("before time", 555)
                    elif time > end_time:
                        self.respond("after time", 666)
                    else:
                        yield function(self, *args, *kwargs)
                else:
                    self.respond("Invalid Token", 401)
            except Exception as e:
                self.respond(e.__str__(), 400)
    return wrapper
