from bson import ObjectId
from tornado.gen import coroutine

from src.authenticate import authenticate
from src.base import BaseHandler


class GraphHandler(BaseHandler):
    @authenticate
    @coroutine
    def get(self, *args, **kwargs):
        company_id = self.get_argument("company")
        token = ObjectId(self.token)
        graph = yield self.db.graph.find_one({"_id": company_id}, {"values": {"$slice": -60}
                                                                   })
        user = yield self.db.user.find_one({"token": token})
        investor = yield self.db.investor.find_one({"_id": user["_id"]})
        share = 0
        for shares in investor["portfolio"]:
            if shares["company"] == company_id:
                share = shares["shares"]
                break
            else:
                share = 0
        for document in graph["values"]:
            document["timestamp"] = document["timestamp"].__str__()[11:16]
        graph["company"] = graph["_id"]
        del graph["_id"]
        self.respond({"graph": graph, "shares": share}, 200)
