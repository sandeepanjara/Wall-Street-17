from tornado.gen import coroutine

from src.admin.admin_authenticate import admin_authenticate
from src.authenticate import authenticate
from src.base import BaseHandler


class PriceHandler(BaseHandler):
    @admin_authenticate
    @coroutine
    def post(self, *args, **kwargs):
        data = self.pars_body()
        price = float(data["price"])
        company_id = data["company"]
        company = yield self.db.company.find_one({"_id": company_id})
        increased = False
        if price >= company["current_price"]:
            increased = True
        if price > company["high"]:
            result = yield self.db.company.update({"_id": company_id},
                                                  {"$set": {"current_price": price, "high": price,
                                                            "increased": increased}})
            if result["nModified"] == 1:
                self.respond("successfully updated", 200)
            else:
                self.respond("not updated", 200)
        elif price < company["low"]:
            result = yield self.db.company.update({"_id": company_id}, {
                "$set": {"current_price": price, "low": price, "increased": increased}})
            if result["nModified"] == 1:
                self.respond("successfully updated", 200)
            else:
                self.respond("not updated", 200)
        else:
            result = yield self.db.company.update({"_id": company_id},
                                                  {"$set": {"current_price": price, "increased": increased}})
            if result["nModified"] == 1:
                self.respond("successfully updated", 200)
            else:
                self.respond("not updated", 200)
