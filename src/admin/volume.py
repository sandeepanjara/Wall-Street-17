from tornado.gen import coroutine

from src.admin.admin_authenticate import admin_authenticate
from src.authenticate import authenticate
from src.base import BaseHandler


class VolumeHandler(BaseHandler):
    @admin_authenticate
    @coroutine
    def post(self, *args, **kwargs):
        data = self.pars_body()
        company_id = data["company"]
        volume = int(data["volume"])
        company = yield self.db.company.find_one({"_id": company_id}, {"current_volume": 1})
        current_volume = company["current_volume"]
        current_volume += volume
        if current_volume >= 0:
            result = yield self.db.company.update({"_id": company_id},
                                                  {"$inc": {"current_volume": volume, "total_volume": volume}})
            if result["nModified"] == 1:
                self.respond("successfully updated", 200)
            else:
                self.respond("not updated", 200)
        else:
            self.respond("volume specified is not available", 200)
