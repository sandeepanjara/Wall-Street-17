from tornado.gen import coroutine

from src.admin.admin_authenticate import admin_authenticate
from src.authenticate import authenticate
from src.base import BaseHandler


class AdminDashboard(BaseHandler):
    @admin_authenticate
    @coroutine
    def get(self, *args, **kwargs):
        companies = list()
        companies_cursor = self.db.company.find()
        if companies_cursor:
            while (yield companies_cursor.fetch_next):
                companies.append(companies_cursor.next_object())
            self.respond(companies, 200)
        else:
            self.respond("Try again", 200)
