from tornado.gen import coroutine

from src.admin.admin_authenticate import admin_authenticate
from src.base import BaseHandler


class AdminLogout(BaseHandler):
    @admin_authenticate
    @coroutine
    def post(self, *args, **kwargs):
        data = self.pars_body()
        user_id = int(data["user_id"])
        result = yield self.db.admin.update({"_id": user_id}, {"$unset": {"token": 1}})
        if result["nModified"] == 1:
            self.respond("Successfully logged out", 200)
        else:
            self.respond("Try again", 400)
