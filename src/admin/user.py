import hashlib
import hmac

from tornado.gen import coroutine

from src.admin.admin_authenticate import admin_authenticate
from src.base import BaseHandler


def getpassword(mobile):
    secret = "".encode("utf-8")
    mobile = str(mobile).encode("utf-8")
    digest = hmac.new(key=secret, msg=mobile, digestmod=hashlib.sha256).hexdigest()
    password = digest[:6]
    return password


class UserHandler(BaseHandler):
    @admin_authenticate
    @coroutine
    def get(self, *args, **kwargs):
        user_id = int(self.get_argument("user_id"))
        user = yield self.db.user.find_one({"_id": user_id}, {"token": 0})
        if user:
            self.respond(user, 200)
        else:
            self.respond("user not found", 200)

    @admin_authenticate
    @coroutine
    def post(self, *args, **kwargs):
        data = self.pars_body()
        user_id = int(data["user_id"])
        name = data["name"]
        try:
            result = yield self.db.user.insert({"_id": user_id, "name": name, "password": getpassword(user_id)})
            if result:
                yield self.db.investor.insert(
                    {"_id": user_id, "name": name, "broker": None, "balance": 1500000, "portfolio": [],
                     "transaction": []})
                self.respond("user successfully inserted", 200)
            else:
                self.respond("user not inserted", 200)
        except Exception as e:
            self.respond("user already exist", 500)
