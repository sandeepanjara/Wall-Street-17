from datetime import datetime
from functools import wraps

from bson import ObjectId
from tornado.gen import coroutine


def admin_authenticate(function):
    @coroutine
    @wraps(function)
    def wrapper(self, *args, **kwargs):
        token = self.request.headers.get("Authorization")
        if token == "null":
            self.respond("Forbidden", 403)
        if token is None:
            self.respond("Forbidden", 403)
        else:
            try:
                token = ObjectId(token)
                result = yield self.db.admin.find_one({"token": token}, {"token": 1})
                if result is not None:
                    yield function(self, *args, *kwargs)
                else:
                    self.respond("Invalid Token", 401)
            except Exception as e:
                self.respond(e.__str__(), 400)

    return wrapper
