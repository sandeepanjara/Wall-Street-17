from datetime import datetime

from tornado.gen import coroutine

from src.admin.admin_authenticate import admin_authenticate
from src.base import BaseHandler


class AddNewsHandler(BaseHandler):
    @admin_authenticate
    @coroutine
    def post(self, *args, **kwargs):
        # TODO
        # make response if not updated
        db = self.settings['client'].wallstreet
        data = self.pars_body()
        if data["broker"] == "general":
            news = dict(
                message=data["message"],
                timestamp=datetime.now()
            )
            result = yield db.news.insert(news)
            if result:
                self.respond("successfully updated news", 200)

        else:
            broker = data["broker"]
            news = dict(
                message=data["message"],
                timestamp=datetime.now()
            )
            result = yield db.broker.update({"_id": broker}, {"$push": {"message": news}})
            if result["nModified"] == 0:
                self.respond("not", 200)
            else:
                self.respond("successfully updated new", 200)
