from tornado.gen import coroutine

from src.admin.admin_authenticate import admin_authenticate
from src.authenticate import authenticate
from src.base import BaseHandler


class CountHandler(BaseHandler):
    @admin_authenticate
    @coroutine
    def get(self, *args, **kwargs):
        users = self.db.user.find()
        count = 0
        while (yield users.fetch_next):
            user = users.next_object()
            try:
                if user["token"]:
                    count += 1
            except KeyError:
                pass
        brokers = self.db.broker.find()
        response = dict()
        while (yield brokers.fetch_next):
            broker = brokers.next_object()
            response[broker["_id"]] = broker["assigned"]
        response["user"] = count
        self.respond(response, 200)
