from tornado.gen import coroutine

from src.authenticate import authenticate
from src.base import BaseHandler


class BrokerHandler(BaseHandler):
    @authenticate
    @coroutine
    def get(self, *args, **kwargs):
        user_id = int(self.get_argument("user_id"))
        broker_list = list()
        brokers = self.db.broker.find({}, {"message": 0, "assigned": 0})
        assigned = yield self.db.investor.find_one({"_id": user_id}, {"broker": 1, "_id": 0})
        while (yield brokers.fetch_next):
            broker = brokers.next_object()
            broker_list.append(broker)
        self.respond({"assigned": assigned["broker"], "brokers": broker_list}, 200)

    @authenticate
    @coroutine
    def post(self, *args, **kwargs):
        data = self.pars_body()
        user_id = int(data["user_id"])
        broker = data["broker"]
        type_of_task = data["type"]
        if type_of_task == "assign":
            investor = yield self.db.investor.find_one({"_id": user_id}, {"balance": 1, "broker": 1, "_id": 0})
            balance = investor["balance"]
            charge = yield self.db.broker.find_one({"_id": broker}, {"charge": 1, "_id": 0})
            charge = charge["charge"]
            if balance < charge:
                self.respond({"message": "Not have enough balance to assign a broker"}, 200)
            else:
                if investor["broker"] is not None:
                    yield self.db.broker.update({"_id": investor["broker"]}, {"$inc": {"assigned": -1}})
                balance -= charge
                yield self.db.investor.update({"_id": user_id}, {"$set": {"broker": broker, "balance": balance}})
                yield self.db.broker.update({"_id": broker}, {"$inc": {"assigned": 1}})
                self.respond({"message": "successfully assigned " + broker, "balance": round(balance, 2)}, 200)
        else:
            data = self.pars_body()
            user_id = int(data["user_id"])
            broker = data["broker"]
            yield self.db.investor.update({"_id": user_id}, {"$set": {"broker": None}})
            yield self.db.broker.update({"_id": broker}, {"$inc": {"assigned": -1}})
            self.respond("successfully removed " + broker, 200)
