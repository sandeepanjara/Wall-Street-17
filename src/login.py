from bson import ObjectId
from tornado.gen import coroutine

from src.base import BaseHandler
import hmac, hashlib


def getpassword(mobile):
    secret = "".encode("utf-8")
    mobile = str(mobile).encode("utf-8")
    digest = hmac.new(key=secret, msg=mobile, digestmod=hashlib.sha256).hexdigest()
    password = digest[:6]
    return password
    # return "password"


class LoginHandler(BaseHandler):
    @coroutine
    def post(self, *args, **kwargs):
        data = self.pars_body()
        user_id = int(data["user_id"])
        password = self.get_request_body_argument("password")
        try:
            if data["type"]:
                result = yield self.db.admin.find_one({"_id": user_id})
                # TODO
                # To make password more secure using hashing
                # Generate new Token at every request
                if result:
                    if password == result["password"]:
                        token = ObjectId()
                        result = yield self.db.admin.update({"_id": user_id}, {"$set": {"token": token}})
                        if result["nModified"] == 1:
                            self.respond(token.__str__(), 200)
                        else:
                            self.respond("try again", 200)
                    else:
                        self.respond("Invalid Password", 401)
                else:
                    self.respond("Invalid User Id", 401)

        except KeyError:
            result = yield self.db.user.find_one({"_id": user_id})
            # TODO
            # To make password more secure using hashing
            # Generate new Token at every request
            if result:
                if password == getpassword(user_id):
                    token = ObjectId()
                    result = yield self.db.user.update({"_id": user_id}, {"$set": {"token": token}})
                    if result["nModified"] == 1:
                        self.respond(token.__str__(), 200)
                    else:
                        self.respond("try again", 200)
                else:
                    self.respond("Invalid Password", 401)
            else:
                self.respond("Invalid User Id", 401)
