from datetime import datetime

from tornado.gen import coroutine

from src.authenticate import authenticate
from src.base import BaseHandler


class TradeHandler(BaseHandler):
    @authenticate
    @coroutine
    def post(self, *args, **kwargs):
        data = self.pars_body()
        self.user_id = int(data["user_id"])
        self.company_id = data["company"]
        self.quantity = int(data["quantity"])
        self.type_of_trade = data["type"]
        company = yield self.db.company.find_one({"_id": self.company_id})
        investor = yield self.db.investor.find_one({"_id": self.user_id})
        broker = yield self.db.broker.find_one({"_id": investor["broker"]})
        self.current_price = company["current_price"]
        self.new_volume = 0
        self.balance = investor["balance"]
        portfolio = investor["portfolio"]
        self.shares = 0
        self.amount = self.current_price * self.quantity
        self.present = False
        self.brokerage = 0
        if not investor["portfolio"]:
            self.present = False
        else:
            for document in investor["portfolio"]:
                if document["company"] == self.company_id:
                    self.shares = document["shares"]
                    self.present = True
        if investor["broker"] is not None:
            self.brokerage = (self.amount * broker["brokerage"]) / 100
            if self.type_of_trade == "buy":
                self.amount += self.brokerage
            else:
                self.amount -= self.brokerage
        else:
            self.amount -= self.brokerage
        current_volume = company["current_volume"]
        if self.type_of_trade == "buy":
            if self.quantity > current_volume:
                self.respond({"message": "Not enough volume"}, 200)
            elif self.amount > self.balance:
                self.respond({"message": "Not enough balance"}, 200)
            else:
                if self.present is False:
                    yield self.db.investor.update({"_id": self.user_id},
                                                  {"$push": {"portfolio": {"company": self.company_id,
                                                                           "shares": self.shares}}})
                try:
                    self.new_volume = current_volume - self.quantity
                    self.shares += self.quantity
                    self.balance -= self.amount
                    if self.make_log() is not None:
                        self.respond({"message": "Successfully bought", "balance": round(self.balance, 2)}, 200)
                except Exception as e:
                    self.respond(e.__str__(), 500)
        else:
            if self.quantity > self.shares or not investor["portfolio"]:
                if self.shares == 0:
                    self.respond({"message": "You have not bought this company's shares"}, 200)
                else:
                    self.respond({"message": "Not have enough shares to sell"}, 200)
            else:
                try:
                    self.new_volume = current_volume + self.quantity
                    self.shares -= self.quantity
                    self.balance += self.amount
                    if self.make_log() is not None:
                        self.respond({"message": "Successfully sold", "balance": round(self.balance, 2)}, 200)
                except Exception as e:
                    self.respond(e.__str__(), 500)

    @coroutine
    def make_log(self):
        yield self.db.company.update({"_id": self.company_id}, {"$set": {"current_volume": self.new_volume}})
        transaction = dict(company=self.company_id,
                           type=self.type_of_trade,
                           timestamp=datetime.now(),
                           price=self.current_price,
                           shares=self.quantity)
        if self.type_of_trade == "sell" and self.shares == 0:
            yield self.db.investor.update({"_id": self.user_id},
                                          {"$set": {"balance": self.balance}}
                                          )
            yield self.db.investor.update({"_id": self.user_id}, {"$pull": {"portfolio": {"company": self.company_id}}})
            result = yield self.db.investor.update({"_id": self.user_id}, {"$push": {"transaction": transaction}})
            return result
        else:
            yield self.db.investor.update({"_id": self.user_id, "portfolio.company": self.company_id},
                                          {"$set": {"balance": self.balance, "portfolio.$.shares": self.shares}}
                                          )
            result = self.db.investor.update({"_id": self.user_id}, {"$push": {"transaction": transaction}})
            return result
