import subprocess
from random import randint

import sys
from motor import MotorClient
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.web import Application

from src.admin.add_message import AddNewsHandler
from src.admin.admin_logout import AdminLogout
from src.admin.count import CountHandler
from src.admin.dashboard import AdminDashboard
from src.admin.price import PriceHandler
from src.admin.user import UserHandler
from src.admin.volume import VolumeHandler
from src.base import BaseHandler
from src.broker import BrokerHandler
from src.dashboard import DashboardHandler
from src.graph import GraphHandler
from src.leaderboard import LeaderBoard
from src.login import LoginHandler
from src.logout import LogoutHandler
from src.news import NewsHandler
from src.portfolio import PortfolioHandler
from src.trade import TradeHandler

client = MotorClient()


class Handle(BaseHandler):
    def get(self, *args, **kwargs):
        self.respond('OK', 200)

application = Application(handlers=[
    (r"/", Handle),
    (r"/login", LoginHandler),
    (r"/dashboard", DashboardHandler),
    (r"/news", NewsHandler),
    (r"/add_news", AddNewsHandler),
    (r"/trade", TradeHandler),
    (r"/portfolio", PortfolioHandler),
    (r"/graph", GraphHandler),
    (r"/broker", BrokerHandler),
    (r"/admin/dashboard", AdminDashboard),
    (r"/admin/price", PriceHandler),
    (r"/admin/volume", VolumeHandler),
    (r"/admin/user", UserHandler),
    (r"/logout", LogoutHandler),
    (r"/count", CountHandler),
    (r"/leaderboard", LeaderBoard),
    (r"/admin/logout", AdminLogout)
],
    client=client
)

if __name__ == '__main__':
    http_server = HTTPServer(application)
    http_server.listen(8000)
    p = subprocess.Popen([sys.executable, 'graph.py'])
    client.wallstreet.process.insert({"graph": p.pid})
    IOLoop.current().start()
