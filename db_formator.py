import csv
import hashlib
import hmac

from pymongo import MongoClient

db = MongoClient("localhost").wallstreet


def getpassword(mobile):
    secret = "dalal-street-secret".encode("utf-8")
    mobile = str(mobile).encode("utf-8")
    digest = hmac.new(key=secret, msg=mobile, digestmod=hashlib.sha256).hexdigest()
    password = digest[:6]
    return password


def user():
    with open("dalal-street.csv") as csvfile:
        rows = csv.DictReader(csvfile)
        for row in rows:
            try:
                db.user.insert(
                    {"_id": int(row["Mobile"]), "name": row["Name"], "password": getpassword(row["Mobile"])})
                db.investor.insert(
                    {"_id": int(row["Mobile"]), "name": row["Name"], "broker": None, "balance": 1500000,
                     "portfolio": [],
                     "transaction": []})
            except Exception as e:
                pass


def company():
    with open("dalal.csv") as csvfile:
        rows = csv.DictReader(csvfile)
        for row in rows:
            row["current_price"] = float(row["current_price"])
            row["total_volume"] = int(row["total_volume"])
            row["high"] = float(row["high"])
            row["low"] = float(row["low"])
            row["current_volume"] = int(row["current_volume"])
            row["increased"] = True
            db.company.insert(row)


def graph():
    documents = db.company.find()
    for document in documents:
        db.graph.insert({"_id": document["_id"], "values": [], "type": document["type"]})


def broker():
    brokers = [{"_id": "Angel Broking", "assigned": 0, "brokerage": 0.8, "prediction_accuracy": 70, "charge": 10000,
                "message": []},
               {"_id": "IndiaBulls", "assigned": 0, "brokerage": 0.3, "prediction_accuracy": 35, "charge": 5000,
                "message": []},
               {"_id": "ShareKhan", "assigned": 0, "brokerage": 0.5, "prediction_accuracy": 50, "charge": 7000,
                "message": []}]
    for br in brokers:
        db.broker.insert(br)


def admin():
    admins = [{"_id": 9999999999, "password": "dalal"}]
    for ad in admins:
        db.admin.insert(ad)


user()
company()
graph()
broker()
admin()
