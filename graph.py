from datetime import datetime
from random import randint
from time import sleep

import pymongo

db = pymongo.MongoClient().wallstreet
while True:
    documents = db.company.find()
    for document in documents:
        price = db.company.find_one({"_id": document["_id"]}, {"current_price": 1})
        db.company.update({"_id": document["_id"]}, {"$set": {"current_price": price["current_price"]}})
        db.graph.update({"_id": document["_id"]},
                        {"$push": {"values": {"timestamp": datetime.now(), "price": price["current_price"]}}})
    sleep(360)
